<?php
	//Define constant variable 
	define('DB_SERVER', 'LOCALHOST');
	define('DB_USER', 'root');
	define('DB_Pass', '');
	define('DB_Name', 'sw');
	$conn = new mysqli(DB_SERVER, DB_USER, DB_Pass, DB_Name); // creating connection
	/**
	 * Class for DB operations
	 */
	class dbConnection
	{
		function __construct() // Creating connection
		{
			$con = mysqli_connect(DB_SERVER,DB_USER,DB_Pass,DB_Name);
			$this->dbh=$con;
			if (mysqli_connect_error()) {
				echo "Failed to connect to DB". mysqli_connect_error();
			}
		}
		// function inserting category with SIZE value
		public function insertSize($SKU, $Name, $Price, $Category, $Size){
			$insertDB = mysqli_query($this->dbh, "INSERT INTO product(sku, name, price, category, size) values('$SKU','$Name', '$Price', '$Category', '$Size')");
			return $insertDB;
		}
		// function inserting category with Dimension values
		public function insertDimension($SKU, $Name, $Price, $Category,  $Width, $Height, $Length ){
			$insertDB = mysqli_query($this->dbh, "INSERT INTO product(sku, name, price, category, width, height, length) values('$SKU','$Name', '$Price', '$Category', '$Width', '$Height', '$Length' )");
			return $insertDB;
		}
		// function inserting category with Weight value
		public function insertWeight($SKU, $Name, $Price, $Category, $Weight ){
			$insertDB = mysqli_query($this->dbh, "INSERT INTO product(sku, name, price, category, weight) values('$SKU','$Name', '$Price', '$Category',  '$Weight' )");
			return $insertDB;
		}
		// function deleting product
		public function productDelete($delete){

			$deleteDB = mysqli_query($this->dbh, "DELETE FROM product WHERE sku = '$delete'");
			return $deleteDB;
		}
	}
?>