<!DOCTYPE HTML>
<html>
<head>

	<title>ScandiWeb Test</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<div>
		<div class="atop">
			<div class="atopleft">
				<h2>Add product</h2>
			</div>
<!-- Top right section for Action selection -->
			<div class="atopright"> 
				<form action="" method="post">
					<input type="submit" value="Add Product">
			</div>
		</div>
<!-- End Top right section -->
<!-- Starting main section -->
		<div class="amain">
			
			<div class="add">
					<?php
						include 'add_execute.php'; // Calling add_execute file 
					?>
					<br>
					SKU: <br>
					<input type="text" name="SKU" > <br>
					Name: <br>
					 <input type="text" name="Name" class="detail"> <br>
					 Price: <br> 
					 <input type="text" name="Price" class="detail"> <br>
					Type Switcher: <br>
					<select name="Type" class="detail" id="add" onchange="adder(this);"> <!-- Calling function show input -->
					<option value="Default">Choose category</option>
					<option value="Size" id="2">Size</option>
					<option value="Dimension" id="3">Dimensions</option>
					<option value="Weight" id="4">Weight</option>
					</select>
					<div id="sizeForm" style="display: none;" class="addForm">
						<br></br>
						<input type="text" id="Size" name="Size" placeholder="Type Size in megabytes!" disabled="true">
						<br></br>
  					</div>
  					<div id="dimensionForm" style="display: none;" class="addForm">
  						<br></br>
						<input type="text" id="Width" name="Width" placeholder="Type Width in centimeters!" disabled="true">
						<br></br>
						<input type="text" id="Height" name="Height" placeholder="Type Height in centimeters!" disabled="true">
						<br></br>
						<input type="text" id="Length" name="Length" placeholder="Type Length in centimeters!" disabled="true">
						<br></br>
  					</div>
					<div id="weightForm" style="display: none;" class="addForm">
						<br></br>
						<input type="text" id="Weight" name="Weight" placeholder="Type Weight in kilograms!" disabled="true">
						<br></br>
  					</div>

				<script>
					//Function hiding and unhiding inputs
					function adder(x){
						
							var x = document.getElementById("add").value;
							if (x == "Size") {
								document.getElementById("sizeForm").style.display = "block";
								document.getElementById('Size').disabled = false;
							}else {
								document.getElementById("sizeForm").style.display = "none";
								document.getElementById('Size').disabled = true;
							}
							if (x == "Dimension") {
								document.getElementById("dimensionForm").style.display = "block";
								document.getElementById('Width').disabled = false;
								document.getElementById('Height').disabled = false;
								document.getElementById('Length').disabled = false;
							}else{
								document.getElementById("dimensionForm").style.display = "none";
								document.getElementById('Width').disabled = false;
								document.getElementById('Height').disabled = false;
								document.getElementById('Length').disabled = false;
							}
							if (x == "Weight") {
								document.getElementById("weightForm").style.display = "block";
								document.getElementById('Weight').disabled = false;
							}else{
								document.getElementById("weightForm").style.display = "none";
								document.getElementById('Weight').disabled = false;
							}
					} 
				</script>
				</form>
				<div id="opt" class="aopt"></div>
			</div>
		</div>
	<!-- End main section -->
	</div>
</body>
</html>