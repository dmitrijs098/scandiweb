
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	include_once("connection.php"); // Calling connection file

	/**
	 *  Creating class Product
	 */
	class Product 
	{
		// Creating class variables
		public $SKU;
		public $name;
		public $price;
		public $category;
		public $size;
		public $witdh;
		public $height;
		public $length;
		public $weight;

		function __construct() // Creating constructor and giving variable value
		{
			$this->SKU = $_POST['SKU'];
			$this->name = $_POST['Name'];
			$this->price = $_POST['Price'];
			$this->category = $_POST['Type'];
			switch ($this->category) {

				case 'Size':
					$this->size = $_POST['Size'];
				break;

				case 'Dimension':
					$this->width = $_POST['Width'];
					$this->height = $_POST['Height'];
					$this->length = $_POST['Length'];
				break;

				case 'Weight':
					$this->weight = $_POST['Weight'];
				break;
				default:
				break;
			}
		}
	}
	$product = new Product(); // Creating object from class Product
	$insertData = new dbConnection(); //Creating object from class dbConnection 

	if (empty($product->SKU)) {
		echo "SKU field can't be empty!";
	}elseif (empty($product->name)) {
		echo "Name field can't be empty!";
	}elseif(empty($product->price)){

		echo "Price can't be empty";
	}else{

		switch ($product->category) {

			case 'Size':
				if (empty($product->size)) {
					echo "Size field can't be empty!";
				}else{
					$sql= $insertData->insertSize($product->SKU, $product->name, $product->price, $product->category, $product->size); // Calling function insertSize and initializing variable value
					if ($sql) {
						echo "Product added";
					}else{
						echo "Product not added";
					}
				}
				
			break;

			case 'Dimension':
				if (empty($product->width)) {
					echo "Width field can't be empty!";
				}elseif (empty($product->height)){
					echo "Height field can't be empty";
				}elseif (empty($product->length)) {
					echo "Length field can't be empty";
				}else{
					$sql= $insertData->insertDimension($product->SKU, $product->name, $product->price, $product->category, $product->width, $product->height, $product->length);// Calling function insertDimension and initializing variable value
					if ($sql) {
						echo "Product added";
					}else{
						echo "Product not added";
					}
				}
			break;

			case 'Weight':
				if (empty($product->weight)) {
					echo "Weight field can't be empty!";
				}else{
					$sql= $insertData->insertWeight($product->SKU, $product->name, $product->price, $product->category, $product->weight);// Calling function insertWeight and initializing variable value
					if ($sql) {
						echo "Product added";
					}else{
						echo "Product not added";
					}
				}
			break;
			default:
				echo "Please choose category!";
			break;
		}
	}
	
}
?>