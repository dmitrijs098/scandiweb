<!DOCTYPE HTML>
<html>
<head>
	<title>ScandiWeb Test</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<div>
		<div class="ltop">
			<div class="ltopleft">
				<h2>Product list</h2>
			</div>
<!-- Top right section for Action selection -->
			<div class="ltopright"> 
				<form action="index.php" method="post">
					<select name="Type" class="detail" id="add" >
					<option value="Default">Mass delete action</option>
					</select>
					<input type="submit" name="submit" value="submit" >
				
			</div>
<!-- End Top right section -->

		</div>
		<div class="lmain">
			<?php

				include 'connection.php'; // Calling connection file
				$sql = "SELECT id, sku, name , price, category, size, width, height, length, weight FROM product";
				$get = $conn->query($sql); //Getting data from DB

				while ($print = mysqli_fetch_array($get)) { // Creating array for each row

					$chValue = $print['sku']; // Creating variable with SKU value
					?>
						<div class="lproduct">
							<?php 
								//Displaying checkbox with SKU value
								echo '<input type="checkbox" name="checkbox[]" value="'. $chValue.'" > '
							?>
							<br>
							<center>
							SKU: <?php echo $print['sku'];?><br>
							<br>
							Name: <?php echo $print['name'];?><br>
							<br>
							Price: <?php echo $print['price'];?><br>
							<br>
							<?php
								switch ($print['category']) {
									case 'Size':
										echo "Size: " . $print['size'] . " MB";
										break;
									case 'Dimension':
										echo "Dimension: " . $print['width']. "x". $print['height']. "x" . $print['length'];
										break;
									case 'Weight':
										echo "Weight: ". $print['weight'] . "KG";
										break;
									
									default:
										echo "Field is empty";
										break;
								}
							?>
						</div>
					<?php
				}
			?>
		</form>
		</div>
		<?php
			$deleteData = new dbConnection(); // Creating new object class dbConnection
			if (isset($_POST['submit'])) {
				$checkbox = $_POST['checkbox'];
				$i = 0; // Creating counter variable
				while ($i < sizeof($checkbox)) {
					$sql= $deleteData->productDelete($checkbox[$i]); // Calling object and product delete function.
					header("Refresh:0"); // Refreshing page
					$i++;
				}
			}
		?>
	</div>

</body>
</html>